using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ObstaclesGenerator : MonoBehaviour {

    [Header("References")]
    public Transform gridOrigin;
    [Header("Obstacles")]
    public GameObject[] obstacles;
    [Header("Grid Config")]
    public int width;
    public int height;
    public float cellSize;
    public Vector2[] invalidGridPositions;
    [Header("DEBUG")]

    private ObstacleGrid grid;

    void Start() {
        grid = new ObstacleGrid(width, height, cellSize, gridOrigin.position);
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.Space)) {
            Vector2 gridPosition = GetRandomGridPosition();
            GenerateObstacle((int) gridPosition.x, (int) gridPosition.y);
        }
    }

    /// <summary>
    /// Generates a random obstacle at a specified x, y grid position
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    private void GenerateObstacle(int x, int y) {
        Vector3 obstaclePosition = grid.GetWorldPosition(x, y);
        if(IsValidXYPosition(x, y)) {
            Debug.Log("Valid");
            Instantiate(obstacles[GetRandomObstacleIndex()], obstaclePosition, Quaternion.identity);
        } else {
            Debug.Log("Invalid");
        }
        grid.SetValue(x, y, 1);
    }

    private bool IsValidXYPosition(int x, int y) {
        if(grid.GetValue(x, y) == 1 && !CheckXYInvalidList(x, y)) {
            return false;
        } else {
            return true;
        }
    }

    private bool CheckXYInvalidList(int x, int y)
    {
        bool isValid = true;
        for(int i = 0; i < invalidGridPositions.Length; i++)
        {
            if ((int)invalidGridPositions[i].x == x && (int)invalidGridPositions[i].y == y)
            {
                isValid = false;
                Debug.Log(invalidGridPositions[i].x + " = " + x + invalidGridPositions[i].y + " = " + y);
                break;
            }
        }

        return isValid;
    }

    /// <summary>
    /// Generates a random obstacle array index
    /// </summary>
    /// <returns></returns>
    private int GetRandomObstacleIndex() {
        return Random.Range(0, obstacles.Length);
    }

    /// <summary>
    /// Generates random grid position
    /// </summary>
    /// <returns></returns>
    private Vector2 GetRandomGridPosition() {
        return new Vector2(Random.Range(0, width), Random.Range(0, height));
    }
    
}
