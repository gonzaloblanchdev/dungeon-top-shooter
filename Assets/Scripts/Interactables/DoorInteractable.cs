using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteractable : InteractableBase {

    [Header("Debug")]
    public BoxCollider2D bCollider;

    private void OnDrawGizmos() {
        Gizmos.color = Color.white;

        if(bCollider != null ) {
            Gizmos.DrawWireCube(transform.position, bCollider.size);
        }
    }

    protected override void Interact() {
        GameManager.instance.PlayerHasInteractedWithDoor();
        Debug.Log("Door");
    }

}
