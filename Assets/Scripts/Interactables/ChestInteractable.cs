using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestInteractable : InteractableBase
{
    public Animator animator;

    protected override void Interact() {
        animator.SetBool("OpenChest", true);
    }
}
