using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGrid {

    private int width;
    private int height;
    private float cellSize;
    Vector3 originPosition;
    private int[,] gridArray;

    public ObstacleGrid(int width, int height, float cellSize, Vector3 originPosition) {
        // Attributes asignation
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        this.originPosition = originPosition;
        // Array initialization
        gridArray = new int[width, height];

        for(int x = 0; x < gridArray.GetLength(0); x++) {
            for(int y = 0; y < gridArray.GetLength(1); y++) {
                Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x, y + 1), Color.white, 100f);
                Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x + 1, y), Color.white, 100f);
            }
        }
        Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width, height), Color.white, 100f);
        Debug.DrawLine(GetWorldPosition(width, 0), GetWorldPosition(width, height), Color.white, 100f);
    }


    public Vector3 GetWorldPosition(int x, int y) {
        return new Vector3(x, y, 0) * cellSize + originPosition;
    }

    private void GetXY(Vector3 worldPosition, out int x, out int y) {
        x = Mathf.FloorToInt((worldPosition - originPosition).x / cellSize);
        y = Mathf.FloorToInt((worldPosition - originPosition).y / cellSize);
    }

    /// <summary>
    /// Sets a value on the x, y position
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="value"></param>
    public void SetValue(int x, int y, int value) {
        if (x >= 0 && y >= 0 && x < width && y < height) {
            gridArray[x, y] = value;
            Debug.Log(gridArray[x, y]);
        }
    }

    /// <summary>
    /// Sets a cell value using at a world position
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <param name="value"></param>
    public void SetValue(Vector3 worldPosition, int value) {
        int x, y;
        GetXY(worldPosition, out x, out y);
        SetValue(x, y, value);
    }

    /// <summary>
    /// Returns the value at a specified x, y position
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public int GetValue(int x, int y) {
        if (x >= 0 && y >= 0 && x < width && y < height) {
            return gridArray[x, y];
        } else {
            return 0;
        }
    }

    /// <summary>
    /// Returns the value at the closests worldPosition cell
    /// </summary>
    /// <param name="worldPosition"></param>
    /// <returns></returns>
    public int GetValue(Vector3 worldPosition) {
        int x, y;
        GetXY(worldPosition, out x, out y);
        return GetValue(x, y);
    }

}
