using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    [Header("References")]
    public Rigidbody2D rb;
    public Transform body;
    [Header("Config")]
    public float bulletSpeed = 10f;
    [Header("Raycast Collisions")]
    public float RayLenght = 0.1f;
    public float rayXOffset = 0.1f;
    public float rayYOffset = 0.1f;
    public LayerMask bulletCollisionLayer;
    [Header("DEBUG")]
    [Space]
    public bool showRayCast = true;
    public Color rayCastGizmoColor = Color.red;
    [Space]
    public bool enableDestroyTime = false;
    public float destroyTime = 2f;
    [Space]
    public bool move = false;

    // PRIVATES
    private Vector2 _velocity;
    private Vector2 _currentDirection;
    private float _currentDestroyTime;

    public Vector2 initialDirection {
        set { _currentDirection = value; }
    }

    private void Start() {
        // TESTING
        //_currentDirection = transform.up;
        _currentDestroyTime = destroyTime;
    }

    private void FixedUpdate() {

        // Collision RayCasts
        bool up = CheckCollision(transform.up, rayYOffset);
        bool down = CheckCollision(-transform.up, rayYOffset);
        bool right = CheckCollision(transform.right, rayXOffset);
        bool left = CheckCollision(-transform.right, rayXOffset);

        if(left || right) {
            _currentDirection.x *= -1;
        }

        if (up || down){
            _currentDirection.y *= -1;
        }

        if (move) {
            if (enableDestroyTime) {
                DestroyTimer();
            }
            CalculateMovement(_currentDirection);
            FixBodyRotation();
            rb.velocity = _velocity;
        }

    }

    private void LateUpdate() {
        
    }

    private void OnDrawGizmos() {
        Gizmos.color = rayCastGizmoColor;

        if (showRayCast) {
            DrawRayGizmo(transform.right, rayXOffset);
            DrawRayGizmo(transform.up, rayYOffset);
            DrawRayGizmo(-transform.right, rayXOffset);
            DrawRayGizmo(-transform.up, rayYOffset);
        }
    }

    /// <summary>
    /// Dibuja un rayo en la direcci�n propuesta
    /// </summary>
    /// <param name="dir"></param>
    private void DrawRayGizmo(Vector3 dir, float rayOffset) {
        Vector3 from = transform.position + dir * rayOffset;
        Vector3 to = from + dir * RayLenght;
        Gizmos.DrawLine(from, to);
    }

    /// <summary>
    /// Calcula el movimiento de la bala
    /// </summary>
    private void CalculateMovement(Vector2 dir) {
        _velocity = dir * bulletSpeed;
    }

    // TODO: ARREGLAR LA ROTACI�N DEL BODY
    private void FixBodyRotation() {
        
    }

    private bool CheckCollision(Vector2 dir, float rayOffset) {
        Vector3 from = transform.position + (Vector3)dir * rayOffset;
        RaycastHit2D hit = Physics2D.Raycast(from, dir, RayLenght, bulletCollisionLayer);

        if(hit.transform == null || hit.transform == transform) {
            return false;
        }

        if (hit.transform.CompareTag("Enemy")) {
            Debug.Log("Enemy");
        }

        return true;
    }

    private void DestroyTimer() {
        _currentDestroyTime -= Time.deltaTime;

        if (_currentDestroyTime <= 0){
            Destroy(this.gameObject);
        }
    }
}
