using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunShoot : MonoBehaviour {

    [Header("References")]
    public GameObject bulletPrefab;
    public CameraShake camShake;
    public Animator gunAnimator;
    public ParticleSystem gunShootParticles;
    [Header("CameraShake config")]
    public float duration = .15f;
    public float magnitude = .4f;

    // PRIVATES
    private Transform _parentTransform;

    private void Awake() {
        _parentTransform = GetComponentInParent<Transform>();
    }

    public void Shoot() {
        GameObject bullet = Instantiate(bulletPrefab, transform.position, _parentTransform.rotation);
        BulletMovement bMovement = bullet.GetComponent<BulletMovement>();
        bMovement.initialDirection = transform.right;
        bMovement.move = true;
        gunAnimator.SetTrigger("Shoot");
        gunShootParticles.Play();
        StartCoroutine(camShake.Shake(duration, magnitude));
    }
}
