using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunLookAtMouse : MonoBehaviour {

    void Update() {
        // Target
        Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // Calculamos el �ngulo que debe rotar en z
        float angleRad = Mathf.Atan2(cursorPosition.y - transform.position.y, cursorPosition.x - transform.position.x);
        // Lo pasamos de radianes a grados decimales
        float angleDegrees = (180 / Mathf.PI) * angleRad;

        // Aplicamos la rotaci�n
        transform.rotation = Quaternion.Euler(0, 0, angleDegrees);
    }
}
