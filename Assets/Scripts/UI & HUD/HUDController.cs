using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour {

    public Image[] hearts;

    private int _currentHeartIndex;

    private void Start() {
        _currentHeartIndex = hearts.Length - 1;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.P)) {
            UpdateHealthInfo(1f);
        }
    }

    public void UpdateHealthInfo(float currentHealth) {
        ClearHeatsFillAmount();

        for(int i = 1; i <= hearts.Length; i++) {
            if(i <= currentHealth) {
                hearts[i - 1].fillAmount = 1;
                Debug.Log("Hello");
                if(i == currentHealth) {
                    break;
                }
            } else {
                float diff = i - currentHealth;
                hearts[i - 1].fillAmount = diff;
                break;
            }
        }
    }

    private void ClearHeatsFillAmount() {
        foreach(Image im in hearts) {
            im.fillAmount = 0;
        }
    }

}
