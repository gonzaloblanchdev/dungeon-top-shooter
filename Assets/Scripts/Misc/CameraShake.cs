using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {
    
    public IEnumerator Shake(float duration, float magnitude) {
        Vector3 originalPosition = transform.localPosition;

        float elapsed = 0f; 

        while(elapsed < duration) {
            float x = Random.Range(-1, 1) * magnitude;
            float y = Random.Range(-1, 1) * magnitude;

            transform.position = new Vector3(x, y, originalPosition.z);

            elapsed += Time.deltaTime;

            // Hace que el bucle se ejecute por cada tick del update
            yield return null;
        }

        transform.localPosition = originalPosition; 
    }

}
