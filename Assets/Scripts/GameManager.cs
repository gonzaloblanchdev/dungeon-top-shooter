using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // PUBLICS
    [Header("References")]
    public Transform playerTransform;
    public PlayerMovement playerMovement;
    public CameraFade camFade;
    [Header("Level Transition")]
    public Transform levelStartPosition;
    public float transitionTime = 1f;
    [Header("Level Chest")]
    public Animator chestAnim;

    // PRIVATES
    private float _currentTransitionTime;
    private bool _isTransitioning;

    public static GameManager instance;

    private void Awake() {
        if(instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
        }
    }

    private void Start() {
        _currentTransitionTime = transitionTime;
    }

    private void Update() {
        if (_isTransitioning) {
            playerMovement.canMove = false;
            TransitionTimer();
        }
    }

    public void PlayerHasInteractedWithDoor() {
        _isTransitioning = true;
        ResetLevel();
        camFade.StartFade();
    }

    private void ResetLevel()
    {
        chestAnim.SetBool("OpenChest", false);
    }

    private void TransitionTimer() {
        _currentTransitionTime -= Time.deltaTime;

        if(_currentTransitionTime <= 0) {
            playerTransform.position = levelStartPosition.position;
            playerMovement.canMove = true;
            camFade.StartFade();
            _isTransitioning = false;
            _currentTransitionTime = transitionTime;
        }
    }
}
