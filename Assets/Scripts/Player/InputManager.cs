using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private PlayerInput playerInput;
    public PlayerInput.MovementActions onMove;
    public PlayerInput.GunsActions guns;
    private PlayerMovement movement;
    private GunShoot gunShoot;
    private PlayerInteactionSystem interact;

    private void Awake() {
        playerInput = new PlayerInput();
        onMove = playerInput.Movement;
        guns = playerInput.Guns;

        movement = GetComponent<PlayerMovement>();
        gunShoot = GetComponentInChildren<GunShoot>();
        interact = GetComponent<PlayerInteactionSystem>();

        guns.Shoot.performed += ctx => gunShoot.Shoot();
    }

    private void OnEnable() {
        onMove.Enable();
        guns.Enable();
    }

    private void OnDisable() {
        onMove.Disable();
        guns.Disable();
    }

    private void Update() {
        movement.CalculateMovement(onMove.Walk.ReadValue<Vector2>());
        movement.UpdateAnimation(onMove.Walk.ReadValue<Vector2>());
        interact.GetMovementDirection(onMove.Walk.ReadValue<Vector2>());
    }
}
