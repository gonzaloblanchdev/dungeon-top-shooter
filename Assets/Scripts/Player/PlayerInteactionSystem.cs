using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteactionSystem : MonoBehaviour {
    
    // PUBLICS
    public float interactionRayDistance = 3f;
    public float interactionRayOffset = 0;

    // PRIVATES
    private InputManager input;
    private Vector2 _rayDirection;

    void Start() {
        input = GetComponent<InputManager>();
    }

    void Update() {
        CheckInteractable(_rayDirection, interactionRayOffset);
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.white;

        DrawRayGizmo((Vector3) _rayDirection, interactionRayOffset);
    }

    /// <summary>
    /// Draws a ray gizmo
    /// </summary>
    /// <param name="dir"></param>
    /// <param name="rayOffset"></param>
    private void DrawRayGizmo(Vector3 dir, float rayOffset) {
        Vector3 from = transform.position + dir * rayOffset;
        Vector3 to = from + dir * interactionRayDistance;
        Gizmos.DrawLine(from, to);
    }

    /// <summary>
    /// Checks for interactables in a determinated direction
    /// </summary>
    /// <param name="dir"></param>
    /// <param name="rayOffset"></param>
    /// <returns></returns>
    private bool CheckInteractable(Vector2 dir, float rayOffset) {
        Vector3 from = transform.position + (Vector3) dir * rayOffset;
        RaycastHit2D hit = Physics2D.Raycast(from, dir, interactionRayDistance);

        if(hit.transform == null || hit.transform == transform) {
            return false;
        }

        if(hit.transform.gameObject.CompareTag("Interactable") && input.onMove.Interact.triggered) {
            InteractableBase interactable = hit.transform.gameObject.GetComponent<InteractableBase>();
            interactable.BaseInteract();
        }

        return true;
    }

    public void GetMovementDirection(Vector2 input) {
        // Player moving right
        if (input.x > 0 && input.y == 0) {

            _rayDirection = transform.right;

            // Player moving left
        } else if (input.x < 0 && input.y == 0) {

            _rayDirection = -transform.right;

            // Player moving back
        } else if (input.x == 0 && input.y > 0) {

            _rayDirection = transform.up;

            // Player moving front
        } else if (input.x == 0 && input.y < 0) {

            _rayDirection = -transform.up;
        }
    }
}
