using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // PUBLICS
    [Header("References")]
    public Rigidbody2D rb;
    public Animator animator;
    [HideInInspector]
    public bool canMove = true;
    [Header("Movement Config")]
    public float moveSpeed = 5f;

    // PRIVATES
    private Vector2 _velocity;

    private void FixedUpdate() {
        if (canMove) {
            rb.velocity = _velocity;
        } else {
            rb.velocity = Vector2.zero;
        }
    }

    public void CalculateMovement(Vector2 input) {
        _velocity = input.normalized * moveSpeed;
        //UpdateAnimation(input); 
    }

    public void UpdateAnimation(Vector2 input) {
        animator.SetFloat("InputX", input.x);
        animator.SetFloat("InputY", input.y);

        // Player moving right
        if(input.x > 0 && input.y == 0) {

            animator.SetBool("WalkRight", true);
            animator.SetBool("WalkLeft", false);
            animator.SetBool("WalkFront", false);
            animator.SetBool("WalkBack", false);

            // Player moving left
        }else if (input.x < 0 && input.y == 0) {

            animator.SetBool("WalkRight", false);
            animator.SetBool("WalkLeft", true);
            animator.SetBool("WalkFront", false);
            animator.SetBool("WalkBack", false);

            // Player moving back
        }else if(input.x == 0 && input.y > 0) {

            animator.SetBool("WalkRight", false);
            animator.SetBool("WalkLeft", false);
            animator.SetBool("WalkFront", false);
            animator.SetBool("WalkBack", true);

            // Player moving front
        }else if(input.x == 0 && input.y < 0) {

            animator.SetBool("WalkRight", false);
            animator.SetBool("WalkLeft", false);
            animator.SetBool("WalkFront", true);
            animator.SetBool("WalkBack", false);
        }
    }
}
