using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    public float maxHealth = 3f;

    private float _currentHealth;

    private void Start() {
        _currentHealth = maxHealth;
    }

    public void PlayerDamaged(float damage) {
        _currentHealth -= damage;

        if(_currentHealth <= 0) {
            Die();
        }
    }

    public void Die() {
        // TODO: Notify Game Manager
    }

}
