using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Chase-Direct Chase", menuName ="Enemy Logic/Chase Logic/DirectChase")]
public class EnemyChaseDirectToPlayer : EnemyChaseSOBase {

    [SerializeField] private float _movementSpeed = 1.75f;

    public override void DoAnimationTriggerEventLogic(Enemy.AnimationTriggerType triggerType) {
        base.DoAnimationTriggerEventLogic(triggerType);
    }

    public override void DoEnterLogic() {
        base.DoEnterLogic();
    }

    public override void DoExitLogic() {
        base.DoExitLogic();
    }

    public override void DoFrameUpdateLogic() {
        base.DoFrameUpdateLogic();

        Vector2 moveDirection = (playerTransform.position - enemy.transform.position).normalized;

        enemy.MoveEnemy(moveDirection * _movementSpeed);

        if (enemy.isWithinStrikingDistance) {
            enemy.stateMachine.ChangeState(enemy.attackState);
        }
    }

    public override void DoPhysiscsLogic() {
        base.DoPhysiscsLogic();
    }

    public override void Initialize(GameObject gameObject, Enemy enemy) {
        base.Initialize(gameObject, enemy);
    }

    public override void ResetValues() {
        base.ResetValues();
    }
}
