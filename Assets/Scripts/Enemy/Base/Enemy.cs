using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IDamagable, IEnemyMovable, ITriggerCheckable {

    [field: SerializeField]
    public float maxHealth { get; set; } = 100f; // Enemy max health
    public float currentHealth { get; set; } // Enemy current healt
    public Rigidbody2D rb { get; set; } // Enemy rigidbody
    public bool isFacingRight { get; set; } = false; 
    public bool isAggroed { get; set; }
    public bool isWithinStrikingDistance { get; set; }

    #region State Machine Variables

    public EnemyStateMachine stateMachine { get; set; }
    public EnemyIdleState idleState { get; set; }
    public EnemyChaseState chaseState { get; set; }
    public EnemyAttackState attackState { get; set; }


    #endregion

    #region ScriptableObject Variables

    [SerializeField] private EnemyIdleSOBase enemyIdle;
    [SerializeField] private EnemyChaseSOBase enemyChase;
    [SerializeField] private EnemyAttackSoBase enemyAttack;

    // Instaces in order to not change all at once
    public EnemyIdleSOBase enemyIdleInstance { get; set; }
    public EnemyChaseSOBase enemyChaseInstance { get; set; }
    public EnemyAttackSoBase enemyAttackInstance { get; set; }

    #endregion


    private void Awake() {

        // Creating SO states instances
        enemyIdleInstance = Instantiate(enemyIdle);
        enemyChaseInstance = Instantiate(enemyChase);
        enemyAttackInstance = Instantiate(enemyAttack);

        // Creating statemachine and states objects
        stateMachine = new EnemyStateMachine();

        idleState = new EnemyIdleState(this, stateMachine);
        chaseState = new EnemyChaseState(this, stateMachine);
        attackState = new EnemyAttackState(this, stateMachine);
    }

    private void Start() {
        currentHealth = maxHealth;

        rb = GetComponent<Rigidbody2D>();

        enemyIdleInstance.Initialize(gameObject, this);
        enemyChaseInstance.Initialize(gameObject, this);
        enemyAttackInstance.Initialize(gameObject, this);

        stateMachine.Initialize(idleState);
    }

    private void Update() {
        stateMachine.currentEnemyState.FrameUpdate();
    }

    private void FixedUpdate() {
        stateMachine.currentEnemyState.PhysiscsUpdate();
    }

    #region Health Functions

    public void Damage(float damageAmount) {
        currentHealth -= damageAmount;

        if(currentHealth <= 0f) {
            Die();
        }
    }

    public void Die() {
        Destroy(gameObject);
    }

    #endregion

    #region Movement Functions

    public void MoveEnemy(Vector2 velocity) {
        rb.velocity = velocity;
        CheckForLeftOrRightFacing(velocity);
    }

    public void CheckForLeftOrRightFacing(Vector2 velocity) {

        if(isFacingRight && velocity.x < 0f) {

            Vector3 rotator = new Vector3(transform.rotation.x, 180f, transform.rotation.z);
            transform.rotation = Quaternion.Euler(rotator);
            isFacingRight = !isFacingRight;

        }else if(!isFacingRight && velocity.x > 0f) {

            Vector3 rotator = new Vector3(transform.rotation.x, 0f, transform.rotation.z);
            transform.rotation = Quaternion.Euler(rotator);
            isFacingRight = !isFacingRight;
        }
    }

    #endregion

    #region Animation Triggers

    private void AnimationTriggerEvent(AnimationTriggerType triggerType) {
        stateMachine.currentEnemyState.AnimationTriggerEvent(triggerType);
    }

    
    public enum AnimationTriggerType {
        EnemyDamaged,
        PlayFootStepsSound
    }

    #endregion

    #region Distance Checks

    public void SetAggroStatus(bool _isAggroed) {
        isAggroed = _isAggroed;
    }

    public void SetStrinkingDistanceBool(bool _isWithinStrikingDistance) {
        isWithinStrikingDistance = _isWithinStrikingDistance;
    }

    #endregion

}
