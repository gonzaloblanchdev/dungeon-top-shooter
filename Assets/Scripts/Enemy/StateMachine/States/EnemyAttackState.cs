using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackState : EnemyState {
    public EnemyAttackState(Enemy enemy, EnemyStateMachine enemyStateMachine) : base(enemy, enemyStateMachine) {
    }

    public override void AnimationTriggerEvent(Enemy.AnimationTriggerType triggerType) {
        base.AnimationTriggerEvent(triggerType);

        enemy.enemyAttackInstance.DoAnimationTriggerEventLogic(triggerType);
    }

    public override void EnterState() {
        base.EnterState();

        enemy.enemyAttackInstance.DoEnterLogic();
    }

    public override void ExitState() {
        base.ExitState();

        enemy.enemyAttackInstance.DoExitLogic();
    }

    public override void FrameUpdate() {
        base.FrameUpdate();

        enemy.enemyAttackInstance.DoFrameUpdateLogic();
    }

    public override void PhysiscsUpdate() {
        base.PhysiscsUpdate();

        enemy.enemyAttackInstance.DoPhysiscsLogic();
    }
}
