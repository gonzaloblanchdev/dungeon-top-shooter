using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIdleState : EnemyState {

    

    public EnemyIdleState(Enemy enemy, EnemyStateMachine enemyStateMachine) : base(enemy, enemyStateMachine) {
    }

    public override void AnimationTriggerEvent(Enemy.AnimationTriggerType triggerType) {
        base.AnimationTriggerEvent(triggerType);

        enemy.enemyIdleInstance.DoAnimationTriggerEventLogic(triggerType);
    }

    public override void EnterState() {
        base.EnterState();

        enemy.enemyIdleInstance.DoEnterLogic();
    }

    public override void ExitState() {
        base.ExitState();

        enemy.enemyIdleInstance.DoExitLogic();
    }

    public override void FrameUpdate() {
        base.FrameUpdate();

        enemy.enemyIdleInstance.DoFrameUpdateLogic();
    }

    public override void PhysiscsUpdate() {
        base.PhysiscsUpdate();

        enemy.enemyIdleInstance.DoPhysiscsLogic();
    }

    
}
