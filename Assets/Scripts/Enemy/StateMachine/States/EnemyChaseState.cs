using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChaseState : EnemyState {

    

    public EnemyChaseState(Enemy enemy, EnemyStateMachine enemyStateMachine) : base(enemy, enemyStateMachine) {
        
    }

    public override void AnimationTriggerEvent(Enemy.AnimationTriggerType triggerType) {
        base.AnimationTriggerEvent(triggerType);

        enemy.enemyChaseInstance.DoAnimationTriggerEventLogic(triggerType);
    }

    public override void EnterState() {
        base.EnterState();

        enemy.enemyChaseInstance.DoEnterLogic();
    }

    public override void ExitState() {
        base.ExitState();

        enemy.enemyChaseInstance.DoExitLogic();
    }

    public override void FrameUpdate() {
        base.FrameUpdate();

        enemy.enemyChaseInstance.DoFrameUpdateLogic();
    }

    public override void PhysiscsUpdate() {
        base.PhysiscsUpdate();

        enemy.enemyChaseInstance.DoPhysiscsLogic();
    }
}
