using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStrickingDistanceCheck : MonoBehaviour {

    public GameObject playerTarget;
    private Enemy _enemy;

    private void Awake() {
        playerTarget = GameObject.FindGameObjectWithTag("Player");
        _enemy = GetComponentInParent<Enemy>();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject == playerTarget) {
            _enemy.SetStrinkingDistanceBool(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject == playerTarget) {
            _enemy.SetStrinkingDistanceBool(false);
        }
    }


}
