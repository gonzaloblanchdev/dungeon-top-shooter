using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyMovable {

    public Rigidbody2D rb { get; set; }
    public bool isFacingRight { get; set; }

    public void MoveEnemy(Vector2 velocity);
    public void CheckForLeftOrRightFacing(Vector2 velocity);

}
