using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITriggerCheckable {

    bool isAggroed { get; set; }
    bool isWithinStrikingDistance { get; set; }

    void SetAggroStatus(bool _isAggroed);
    void SetStrinkingDistanceBool(bool _isWithinStrikingDistance);

}
